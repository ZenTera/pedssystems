# Author: Lucas Zenichi Terada
# Institution: University of Campinas

import tkinter as tk
from PIL import Image, ImageTk
from gui import tools

TITLE_FONT = ('Verdana', 20, 'bold')
LARGE_FONT = ("Verdana", 18)

# Define default settings for application
class application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.geometry("800x700")
        container = tk.Frame(self)
        container.config(bg='white')
        container.pack(side="top", fill="both", expand="True")

        self.frames = {}
        for F in (startpage, powerfpage,restpage,recpage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.config(bg='white')
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(startpage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class startpage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        render = ImageTk.PhotoImage(Image.open('gui/logo.png'))
        logo = tk.Label(self, image=render, background='white')
        logo.image = render
        label = tk.Label(self,text='Python Energy Distribution \n Solutions',font=TITLE_FONT,background='white')
        buttonpf = tk.Button(self, **tools.bgreen('PowerFlow'),command=lambda:controller.show_frame(powerfpage))
        buttonrt = tk.Button(self, **tools.bgreen('Restoration'),command=lambda: controller.show_frame(recpage))
        buttonrc = tk.Button(self, **tools.bgreen('Reconfiguration'),command=lambda: controller.show_frame(restpage))
        cancel   = tk.Button(self, **tools.bred('Cancel'),command=lambda: self.quit())

        labelpfw = tk.Label(self, text="Solve Power Flow Problems", **tools.label())
        labelrtn = tk.Label(self, text="Solve Restoration Problems", **tools.label())
        labelrce = tk.Label(self, text="Solve Reconfiguration Problems", **tools.label())

        logo.grid(row=1, column=1, padx=20, pady=20,sticky='n')
        label.grid(row=1, column=2, padx=20, pady=20,sticky='n')
        buttonpf.grid(row=2, column=1, padx=20, pady=20,sticky='n')
        labelpfw.grid(row=2, column=2, padx=20, pady=20,sticky='w')
        buttonrt.grid(row=3, column=1, padx=20, pady=20,sticky='n')
        labelrtn.grid(row=3, column=2, padx=20, pady=20,sticky='w')
        buttonrc.grid(row=4, column=1, padx=20, pady=20,sticky='n')
        labelrce.grid(row=4, column=2, padx=20, pady=20,sticky='w')
        cancel.grid(  row=5, column= 1, padx=20, pady=20,sticky='n')

class powerfpage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)


        button1 = tk.Button(self, text="back to home",
                            command=lambda: controller.show_frame(startpage))
        button1.pack()

class restpage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label1 = tk.Label(self, text="Page one", font=LARGE_FONT)
        label1.pack(pady=10, padx=10)
        button1 = tk.Button(self, text="back to home",
                            command=lambda: controller.show_frame(startpage))
        button1.pack()

class recpage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label1 = tk.Label(self, text="Page one", font=LARGE_FONT)
        label1.pack(pady=10, padx=10)
        button1 = tk.Button(self, text="back to home",
                            command=lambda: controller.show_frame(startpage))
        button1.pack()
